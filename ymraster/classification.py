# -*- coding: utf-8 -*-
"""
Created on Wed Feb  4 14:54:42 2015

@author:
"""
import numpy as np
from osgeo import gdal, ogr
from ymraster import Raster, write_file
from raster_dtype import RasterDataType
from sklearn import tree
from sklearn.metrics import confusion_matrix, classification_report,\
                             accuracy_score
from rasterstats import zonal_stats
import scipy as sp

def get_samples_from_roi(in_rst_label,in_rst_roi,in_rst_stat ):
    '''
    The function, thanks to a label image, picks the index of one pixel per
    sample in a sample raster. Then it takes for each sample the statistic
    features present in a "statistic raster" and casts it in a 2d matrix. The
    three rasters should be of the same size and the samples should exactly
    match with the segmentation objects of the label image. The input rasters
    could be any file that GDAL can open.

    :param in_rst_label: name of the label image, supposedly created previously
                        during a segmentation.
    :param in_rst_roi: name of the sample raster, all the samples should
                        correspond to a object in the label image
    :param in_rst_stat: name of the statistic features raster of the
                        segmentation objects
    :returns:
            X: the sample matrix. A nXd matrix, where n is the number of
            referenced samples and d is the number of features. Each line of
            the matrix is sample.

            Y: the classes of the samples in a vertical n matrix.
    '''

    ## Open data
    stat = gdal.Open(in_rst_stat,gdal.GA_ReadOnly)
    if stat is None:
        print 'Impossible to open '+ in_rst_stat
        exit()

    roi = gdal.Open(in_rst_roi,gdal.GA_ReadOnly)
    if roi is None:
        print 'Impossible to open '+ in_rst_roi
        exit()

    label = gdal.Open(in_rst_label, gdal.GA_ReadOnly)
    if label is None:
        print 'Impossible to open '+ in_rst_label
        exit()
    ##Test the size
    if not((stat.RasterXSize == roi.RasterXSize) and (stat.RasterYSize ==
    roi.RasterYSize) and (stat.RasterXSize == label.RasterXSize) and
    (stat.RasterYSize == label.RasterYSize) ):
        print 'Images should be of the same size'
        exit()

    ## Get the number of features
    d  = stat.RasterCount

    ## load the ROI array
    ROI = roi.GetRasterBand(1).ReadAsArray()
    t = (ROI == 0).nonzero()

    ##load the label array and set negative value where the objects don't
    #correspond to samples
    LABEL = label.GetRasterBand(1).ReadAsArray()
    LABEL[t] = -99

    ##get the indices of one pixel per sample
    #sort the label by their id and get the indices of the first occurrences
    #of the unique values in the (flattened) original array (LABEL)
    l, l_ind  = np.unique(LABEL,return_index = True)
    #Delete the first id, corresponding to -99
    l_ind = l_ind[1:len(l_ind)]
    nb_samp = len(l_ind)
    col = LABEL.shape[1]
    #Get the index of each sample in the non-flattened original array
    indices = [np.empty(nb_samp),np.empty(nb_samp)]
    indices[0] = [l_ind // col]    #the rows
    indices[1] = [l_ind % col]    #the columns

    ##set the Y array, ie taking the classes values of each sample
    Y = ROI[indices].reshape((nb_samp,1))
    del ROI
    roi = None

    ##set the X array, ie taking all the statistic features for each sample
    try:
        X = np.empty((nb_samp,d))

    except MemoryError:
        print 'Impossible to allocate memory: roi too big'
        exit()

    for i in range(d):
        temp = stat.GetRasterBand(i+1).ReadAsArray()
        X[:,i] = temp[indices]

    #close the files and release memory
    stat = None
    del temp, indices, d, nb_samp, col, t, l, l_ind

    return X,Y

def get_samples_from_label_img(in_rst_label, in_rst_stat):
    """
    The function, given a label and statistic image, compute in a 2d array the
    feature per label.The two input rasters should be of the same size. The
    input rasters could be any file that GDAL can open. The function also
    compute a reverse matrix that can permit to rebuild an image from the
    result of an object classification

    :param in_rst_label: name of the label image, supposedly created previously
                        during a segmentation.
    :param in_rst_stat: name of the statistic features raster of the
                        segmentation objects
    :returns:
            X: the sample matrix. A nXd matrix, where n is the number of
            label and d is the number of features. Each line of
            the matrix is label.

            reverse: everse matrix that can permit to rebuild an image from the
            result of an object classification.
    """
    ## Open data
    stat = gdal.Open(in_rst_stat,gdal.GA_ReadOnly)
    if stat is None:
        print 'Impossible to open '+ in_rst_stat
        exit()

    label = gdal.Open(in_rst_label, gdal.GA_ReadOnly)
    if label is None:
        print 'Impossible to open '+ in_rst_label
        exit()

    ##Test the size
    if not((stat.RasterXSize == label.RasterXSize) and
    (stat.RasterYSize == label.RasterYSize) ):
        print 'Images should be of the same size'
        exit()

    ## Get the number of features
    d  = stat.RasterCount

    ##load the label array
    LABEL = label.GetRasterBand(1).ReadAsArray()

    ##get the indices of one pixel per label
    #sort the label by their id and get the indices of the first occurrences
    #of the unique values in the (flattened) original array (LABEL). Compute
    #also the reverse matrix that can permit to rebuild the original array.
    l, l_ind, reverse  = np.unique(LABEL,return_index = True,
                                   return_inverse = True)
    nb_samp = len(l_ind)

    #Get the index of each sample in the non-flattened original array
    col = LABEL.shape[1]
    indices = [np.empty(nb_samp),np.empty(nb_samp)]
    indices[0] = [l_ind // col]#the rows
    indices[1] = [l_ind % col]#the columns

    ##set the X array, ie taking all the statistic features for each label
    try:
        X = np.empty((nb_samp,d))

    except MemoryError:
        print 'Impossible to allocate memory: label image too big'
        exit()

    for i in range(d):
        temp = stat.GetRasterBand(i+1).ReadAsArray()
        X[:,i] = temp[indices]

    #close the files and release memory
    stat = None
    del temp, indices, d, nb_samp, col, l, l_ind

    return X, reverse
zonal_stats

def decision_tree(X_train, Y_train, X_test, X_img, reverse_array, raster,
                  out_filename, ext = 'Gtiff' ):
    """
    :param X_train: The sample-features matrix used to train the model, a n*d
                    array where n is the number of referenced samples and d is
                    the number of features.
    :param Y_train: The classes of the samples in a vertical n matrix.
    :param X_test: The sample-features matrix corresponding to the validation
                    dataset on which the model is applied, a n*d array where n
                    is the number of referenced samples and d is the number of
                    features.
    :param X_img: The sample-features matrix corresponding to the wole image
                    on which the model is applied, a n*d array where n is the
                    number of referenced samples and d is the number of
                    features.
    :param reverse_array:The reverse matrix use to rebuild into the origin
                        dimension the result of the classification. This matrix
                        is supposed to be computed previously (cf.
                        get_samples_from_label_img() #TODO)
    :param raster: The raster object that contains all the meta-data that should
                    be set on the classification image written, eg : it could be
                    the raster object of the labelled image.
    :param out_filename: Name of the classification image to be written.
    :param ext: Format of the output image to be written. Any formats
                supported by GDAL. The default value is 'Gtiff'.
    :returns:
            y_predict: The classes predicted from the validation dataset, in a
                        vertical n matrix. It is useful to compute prediction
                        error metrics.
    """
    #Get some parameters
    rows = raster.meta['height']
    col = raster.meta['width']

    #Set the DecisionTreeClassifier
    clf = tree.DecisionTreeClassifier()

    #Train the model
    clf = clf.fit(X_train, Y_train)

    #Perform the prediction on the whole label image
    classif = clf.predict(X_img)

    #Perform the prediction on the test sample
    y_predict = clf.predict(X_test)

    #Rebuild the image from the classif flat array with the given reverse array
    classif = classif[reverse_array]
    classif = classif.reshape(rows,col)

    #write the file
    meta = raster.meta
    meta['driver'] = gdal.GetDriverByName(ext)
    meta['dtype'] = RasterDataType(numpy_dtype = np.uint32)
    meta['count'] = None
    write_file(out_filename, overwrite=True, array=classif, **meta)

    return y_predict, clf


def classifier(X_train, Y_train, X_test, X_img):
    """
    Train a classifier and apply the model on a whole dataset.
    Parameters
    ----------
    X_train : ndarray,
        The sample-features matrix used to train the model, a n*d
        array where n is the number of referenced samples and d is
        the number of features.
    Y_train:
        The labels of the samples in a vertical n matrix.
    X_test:
        The sample-features matrix corresponding to the validation
        dataset on which the model is applied, a n*d array where n
        is the number of referenced samples and d is the number of
        features.
    X_img:
        The sample-features matrix corresponding to the wole image
        on which the model is applied, a n*d array where n is the
        number of referenced samples and d is the number of
        features.

    Returns
    -------
    Y_predict_test : The labels predicted from the validation dataset, in a
        vertical n matrix. It is useful to compute prediction
        error metrics.
    Y_img : The labels predicted from the whole image dataset, in a
        vertical n matrix. It is useful to add the result to shapefile
        afterward.
    """
    #Set the DecisionTreeClassifier
    clf = tree.DecisionTreeClassifier()

    #Train the model
    clf = clf.fit(X_train, Y_train)

    #Perform the prediction on the whole label image
    Y_img = clf.predict(X_img)

    #Perform the prediction on the test sample
    Y_predict_test = clf.predict(X_test)

    return Y_predict_test, Y_img



def pred_error_metrics(Y_predict, Y_test, target_names = None):
    """This function calcul the main classification metrics and compute and
    display confusion matrix.

    :param Y_predict: Vertical n matrix correspind to the estimated targets as
                    returned by a classifier.
    :param Y_test: Vertical n matrix corresponding to the ground truth
                    (correct) target values.
    :param target_names: List of string that contains the names of the classes.
                        Default value is None.

    :returns:
            cm : Array of the confusion matrix.
            report : Text summary of the precision, recall, F1 score for each
                    class.
            accuracy: float, If normalize == True, return the correctly
                        classified samples (float), else it returns the number
                        of correctly classified samples (int).
    """


    #Compute the main classification metrics
    report = classification_report(Y_test, Y_predict, target_names = target_names)
    accuracy = accuracy_score(Y_test, Y_predict)

    #Compute the confusion matrix
    cm = confusion_matrix(Y_test, Y_predict)


    return cm, report, accuracy

def get_stats_from_shape(vector, raster, stats, nodata = None, field_id = 'DN',
                         field_class = 'class', **kwargs):
    """

    Parameters
    ----------
    vector: path to an vector source or geo-like python objects

    raster: ndarray or path to a GDAL raster source
        If ndarray is passed, the ``affine`` kwarg is required.
    stats:  list of str,
        Which statistics to calculate for each zone.
        All possible choices are ['count', 'min', 'max', 'mean', 'sum', 'std',
        'median', 'majority', 'minority', 'unique', 'range', 'nodata']
    nodata: float, optional
        If `raster` is a GDAL source, this value overrides any NODATA value
        specified in the file's metadata.
        If `None`, the file's metadata's NODATA value (if any) will be used.
        defaults to `None`.
    field_id : string
        Name of the field containing the unique id of the segments.
    field_class : string,
        Name of the field containing the class number of segments.
    debug : bool,
        Check if field_id contains unique values and print the duplicates if
        any
    Returns
    -------
    X : ndarray
        A nXd matrix, where n is the number of segment and d is the number of
        features (d = n_band * n_stat). Each line of the matrix is a segment.
    Y : 1d array,
        The label matrix where each line correspond to label of a segment, if
        there is no class indicated for a segment, his default label will be 0.
    FID :1d array,
        FID matrix where each line correspond to id of a segment.
    """

    #get params
    rst = Raster(raster)
    n_band = rst.count
    del rst
    init = True
    inconsistency = 0

    #for each band of the raster img
    for idx_band in range(n_band):

        #get stats with rasterstats module
        my_stats  = zonal_stats(vector,raster, stats = stats,
                                band = idx_band + 1, geojson_out = True,
                                nodata = None)

        #convert it to an X an Y array
        #-----------------------------
        if init :   #for the first band, initialise the arrays
            n_feat = len(my_stats)
            X = sp.empty((n_feat,n_band * len(stats)))
            Y = sp.empty((n_feat,1))
            FID = sp.empty((n_feat,1))
            for i,feat in enumerate(my_stats):
                Y[i] = feat['properties'][field_class]
                FID[i] = feat['properties'][field_id]
                for j,s in enumerate(stats):
                    X[i,j + idx_band * len(stats)] = feat['properties'][s]
            init = False
        else:
            for i,feat in enumerate(my_stats):
                if FID[i] != feat['properties'][field_id] :
                    #check if the stats of the differents bands have been
                    #computed in the same order
                    inconsistency += 1
                for j,s in enumerate(stats):
                    X[i,j + idx_band * len(stats)] = feat['properties'][s]

        if inconsistency > 0:
            print 'Inconsistency has been detected {} times'.format(inconsistency)

    if kwargs.get('debug'):
        labels, count = sp.unique(FID, return_counts = True)
        t = sp.where(count != 1)
        for elem in t[0]:
            print 'Warning : Label {} appears {} times'.format(labels[elem], count[elem])
    return X, Y, FID


def add_predict_to_shp(vector, Y_predict, FID, **kwargs):
    """
    Add the predicted classes to the original shapefile.

    Parameters
    ----------
    vector: path to an vector source or geo-like python objects
    Y_predict : 1d array,
        The label matrix where each line correspond to label of a segment, if
        there is no class indicated for a segment, his default label will be 0.
    FID :1d array,
        FID matrix where each line correspond to id of a segment.
    field_pred : string, optional
        Name of the field containing the predicted class number of segments.
        By defaults, the new field's name is 'class_pred'.
    field_id : string, optional
        Name of the field containing the unique id of the segments. Defaults
        to 'DN'.
    """
    field_pred = kwargs.get('field_pred', 'class_pred')
    field_id = kwargs.get('field_id', 'DN')
    source = ogr.Open(vector, update=True)
    layer = source.GetLayer()
    layerDefinition = layer.GetLayerDefn()
    field_names = [layerDefinition.GetFieldDefn(i).GetName() for i in range(layerDefinition.GetFieldCount())]

    # Add a new field
    new_field = ogr.FieldDefn(field_pred, ogr.OFTInteger)
    layer.CreateField(new_field)

    for feat in layer :
        fid = feat.GetField(field_id)
        t = sp.where(FID == fid)[0]
        pred = Y_predict[t]
        try :
            feat.SetField(field_pred, int(pred))
        except TypeError :
            print 'Warning type error : ', pred
        layer.SetFeature(feat)
    source.Destroy()

def extract_sample(X,Y,FID):
    """
    Extract from matrices the lines corresponding to segments that have a label
    (i.e. differents from 0)

    Parameters
    ----------
    X : ndarray
        A nXd matrix, where n is the number of segment and d is the number of
        features (d = n_band * n_stat). Each line of the matrix is a segment.
    Y : 1d array,
        The label matrix where each line correspond to label of a segment, with
        the label 0 for segment that do not have a label
        FID matrix where each line correspond to id of a segment.

    Returns
    -------
    X_samp : ndarray
        The X matrix with only lines corresponding to segment that have a label
    Y_samp : 1d array,
        The Y matrix with only lines corresponding to segment that have a label
    FID_samp :1d array,
        The FID matrix with only lines corresponding to segment that have
        a label.
    """

    #where there is label
    t = sp.where(Y != 0)[0]

    #extract the corresponding matrices
    X_samp = X[t, :]
    Y_samp = Y[t, :]
    FID_samp = FID[t, :]

    return X_samp, Y_samp, FID_samp

def regroup_class(dict_change, *Y):
    """
    Parameters
    ----------
    dict_change: dict {val_to_change : val_to_set},
        Dictionnary that contains the changes to apply. On keys the value
        to change and on 'value' the corresponding value to replace by.
    Y : list of 1d array,
        The matrix(ces) containing the labels to regroup

    Returns
    -------
    Y_regroup : list of 1d array,
        The matrix(ces) containing the labels regrouped.
    """
    Y_regroup = []
    for y in Y:
        y_regroup = sp.copy(y)
        for src, dst in dict_change.iteritems():
            y_regroup[y == src] = dst
        Y_regroup.append(y_regroup)

    return Y_regroup
#    Y_true_regroup = sp.copy(Y_true)
#    Y_pred_regroup = sp.copy(Y_pred)
#    for src, dst in dict_change.iteritems():
#        Y_true_regroup[Y_true == src] = dst
#        Y_pred_regroup[Y_pred == src] = dst

#return Y_true_regroup, Y_pred_regroup

