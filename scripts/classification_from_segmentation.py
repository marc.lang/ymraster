# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 15:14:42 2015

@author: sig
"""
import os
from ymraster import classification as cla
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import cohen_kappa_score
from pprint import pprint


def classification_from_A_to_Z(roi_file, raster, **kwargs):
    """
    Given a vector segmentation with some segments labellised, and the image
    to classify, the fonction train a model, apply it to the wole segmentation
    and returns some quality indices.

    Parameters
    ----------
    vector_segmentation : string,
        Path to an vector source or geo-like python objects
    raster : strin,
        path to a GDAL raster source
    stats :  list of str, optional
        Which statistics to calculate for each zone.
        All possible choices are ['count', 'min', 'max', 'mean', 'sum', 'std',
        'median', 'majority', 'minority', 'unique', 'range', 'nodata'].
        Defaults to ['mean', 'std'].
    nodata : float, optional
        If `raster` is a GDAL source, this value overrides any NODATA value
        specified in the file's metadata.
        If `None`, the file's metadata's NODATA value (if any) will be used.
        defaults to `None`.
    field_id : string, optional
        Name of the field containing the unique id of the segments. Defaults
        to 'DN'.
    field_class : string, optional
        Name of the field containing the class number of segments. Defaults to
        'class'. 0 can not be used as a class number, otherwise
    field_pred : string, optional
        Name of the field containing the predicted class number of segments.
        By defaults, the new field's name is 'class_pred'.
    train_size : int or float, optional
        If float, it must be comprised between 0 and 1, and its the proportion
        of sample to use for training. If int, it is the absolute number of
        sample to use for training. Defaul to 0.5.
    target_names : list of string, (optional)
        List of classes' name.

    Returns
    -------
    dict_output : dictionnary containing the following variables :
        cm : 2d array,
            The confusion matrix, true labels in row and predicted label in columns
        kappa : float,
            Cohen Kappa indice
        accuracy :float,
            Overall accuracy.
        report : string,
            Report of precision, recall, f1-score of each class.
        Y_predict : 1d array,
            Contain the predicted labels of segments used for validation.
        Y_true : 1d array,
            Contain the true labels of segments used for validation.
        classif : 1d array,
            Contain the predicted labels of all segments of `vector_segmentation`
        FID : 1d array,
            Contain the id of all segments of `vector_segmentation`. It
            corresponds to the id of `field_id` field of the shapefile.

    """
    #extract paremeters
    stats = kwargs.get('stats', ['mean','std'])
    field_id = kwargs.get('field_id', 'DN')
    field_class = kwargs.get('field_class', 'class')
    train_size = kwargs.get('train_size', 0.5)
    nodata = kwargs.get('nodata', None)
    target_names = kwargs.get('target_names')
    debug = kwargs.get('debug', False)
    field_pred = kwargs.get('field_pred', 'pred_class')

    pprint(kwargs)

    #extract stats for the whole segmentation
    X, Y, FID = cla.get_stats_from_shape(roi_file, raster,stats = stats,
                                         nodata = nodata, field_id = field_id,
                                         field_class = field_class,
                                         debug = debug)

    #extract stats only for segments that are labellised
    X_samp, Y_samp, FID_samp = cla.extract_sample(X,Y,FID)

    #split them into train and test sample
    X_train, X_test, Y_train, Y_test = train_test_split(X_samp ,Y_samp ,train_size = train_size)

    #Train, then apply a classifier, on a validation set and on the whole
    #segmentation
    Y_predict, classif = cla.classifier(X_train, Y_train, X_test, X)

    #update the vector segmenation
    cla.add_predict_to_shp(roi_file, classif, FID, field_pred = field_pred,
                           field_id = field_id)

    #get some quality indices
    cm, report, accuracy = cla.pred_error_metrics(Y_predict, Y_test,
                                                   target_names = target_names)

    kappa = cohen_kappa_score(Y_predict, Y_test)
    #Print the results
    print "confusion matrix\n", cm
    print report
    print "OA :", accuracy
    print "Kappa:", kappa

    try :
        # Show confusion matrix in a separate window
        plt.matshow(cm)
        plt.title('Confusion matrix')
        plt.colorbar()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()
    except: #in case it called from a server
        print 'Confusion Matrix can not be displayed'

    dict_output = {'cm' : cm,
                   'kappa' : kappa,
                   'accuracy' : accuracy,
                   'report' : report,
                   'Y_predict' : Y_predict,
                   'Y_true' : Y_test,
                   'classif' : classif,
                   'FID' : FID}

    return dict_output

def reclass(vector_segmentation, dict_change, **kwargs):
    """
    The function regroup class after a classification. First it compute the new
    confusion matrix and others precision indices, secondly, it update
    the vector_segmentation with the new classes.

    Parameters
    ----------
    vector_segmentation : string,
        Path to an vector source or geo-like python objects
    dict_change: dict {val_to_change : val_to_set},
        Dictionnary that contains the changes to apply. On keys the value
        to change and on 'value' the corresponding value to replace by.
    Y_predict : 1d array,
        Contain the predicted labels of segments used for validation.
    Y_true : 1d array,
        Contain the true labels of segments used for validation.
    classif : 1d array,
        Contain the predicted labels of all segments of `vector_segmentation`
    FID : 1d array,
        Contain the id of all segments of `vector_segmentation`. It
        corresponds to the id of `field_id` field of the shapefile.
    target_names : list of string, (optional)
        List of classes' name.
    field_pred : string, optional
        Name of the field containing the predicted class number of segments.
        By defaults, the new field's name is 'pred_regroup'.
    field_id : string, optional
        Name of the field containing the unique id of the segments. Defaults
        to 'DN'.
    Returns
    -------
    **dict_output** : dictionnary containing the following variables, taking
    account the new classes.
        cm : 2d array,
            The confusion matrix, true labels in row and predicted label in
            columns
        kappa : float,
            Cohen Kappa indice
        accuracy :float,
            Overall accuracy.
        report : string,
            Report of precision, recall, f1-score of each class.
        Y_predict : 1d array,
            Contain the predicted labels of segments used for validation.
        Y_true : 1d array,
            Contain the true labels of segments used for validation.
        classif : 1d array,
            Contain the predicted labels of all segments of `vector_segmentation`
        FID : 1d array,
            Contain the id of all segments of `vector_segmentation`. It
            corresponds to the id of `field_id` field of the shapefile.
    """

    #extract some parameters
    field_pred = kwargs.get('field_pred', 'regroup_class')
    field_id = kwargs.get('field_id', 'DN')

    #regroup the classes
    Y_true_regroup, Y_pred_regroup, classif_regroup = cla.regroup_class(dict_change,
                                                       kwargs['Y_true'],
                                                       kwargs['Y_predict'],
                                                       kwargs['classif'])


    #update the vector segmentation
    cla.add_predict_to_shp(vector_segmentation, classif_regroup, kwargs['FID'],
                           field_pred = field_pred, field_id = field_id)

    #get some statistics on regrouped classes
    target_names = kwargs.get('target_names',None)
    cm, report, accuracy = cla.pred_error_metrics(Y_pred_regroup, Y_true_regroup,
                                                   target_names = target_names)

    kappa = cohen_kappa_score(Y_pred_regroup, Y_true_regroup)

    #Print the results
    print "confusion matrix\n", cm
    print report
    print "OA :", accuracy
    print "Kappa:", kappa

    dict_output = {'cm' : cm,
                   'kappa' : kappa,
                   'accuracy' : accuracy,
                   'report' : report,
                   'Y_predict' : Y_pred_regroup,
                   'Y_true' : Y_true_regroup,
                   'classif' : classif_regroup,
                   'FID' : kwargs['FID']}

    return dict_output

def main():

    roi_file = '/home/marc/Documents/stage/oiseaux_2017/data/campestre_segm_test.shp'
    raster = '/home/marc/Documents/stage/oiseaux_2017/data/campestre.tif'
    kwargs = {}
    kwargs['stats'] = ['mean','std','min','max']
    #kwargs['nodata'] =
    #kwargs['field_id']  =
    #kwargs['field_class'] =
    kwargs['train_size'] =  0.5
    kwargs['debug'] = True
    #kwargs['target_names'] = ['herbs1','herbs2','buisson','arbre']

    dict_output = classification_from_A_to_Z(roi_file,raster, **kwargs)

    dict_change = { 11 : 10,
                    12 : 10,
                    13 : 10,
                    14 : 10,
                    21 : 20,
                    22 : 20,
                    23 : 20,
                   }

    Y_true_regroup, Y_pred_regroup = cla.regroup_class(dict_output['Y_true'],
                                                       dict_output['Y_predict'],
                                                        dict_change)
    #update the vector segmenation
    cla.add_predict_to_shp(roi_file, classif, dict_output['FID'])

    cm, report, accuracy = cla.pred_error_metrics(Y_pred_regroup, Y_true_regroup,
                                                   target_names = None)

    kappa = cohen_kappa_score(Y_pred_regroup, Y_true_regroup)
    #Print the results
    print "confusion matrix\n", cm
    print report
    print "OA :", accuracy
    print "Kappa:", kappa

if __name__ == "__main__":
    main()
    print 'yolo main'
